# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Mari Carmen Fernández de Pedro
* Titulación: Ingeniería en Sistemas de Telecomunicación
* Cuenta en laboratorios: mfdez
* Cuenta URJC: mc.fernandezd.2017@alumnos.urjc.es
* Video básico (url): https://www.youtube.com/watch?v=UAVuZLVx1hg
* Video parte opcional (url): https://www.youtube.com/watch?v=99dKYqWHlG0
* Despliegue (url): mcfernandez2024.pythonanywhere.com 
* Contraseñas: No hay autenticación de usuarios.
* Cuenta Admin Site: admin/saro2024

## Resumen parte obligatoria
* Al poner la url de mi página web aparece la página principal. En ella aparece un menú desde el que se puede acceder, mediante enlaces, a otras páginas. También aparecerán los comentarios puestos, organizados de más nuevos a más viejos. Para cada comentario se mostrará la información de esa cámara.
* En la página de cámaras aparece un listado de las dos fuentes de datos disponibles y un listado de las 7 cámaras ordenadas por número de comentarios (de más a menos).
* En la página de cada cámara aparece un enlace a la página dinámica, un enlace a la página json, información de la cámara (nombre y localización), imagen de la cámara actual, un listado de todos los comentarios puestos para esa cámara y un enlace a la página que permite poner comentarios.
* En la página dinámica aparece el mismo contenido que en la página de la cámara. Cada 30 segundos se actualiza la imagen de la cámara.
* En la página json aparece el id de la cámara, la url de la imagen, el nombre de la cámara, su localización y el número de comentarios.
* En la página de configuración aparecen un formulario para elegir el nombre del autor de los comentarios y un formulario para elegir el tipo y el tamaño de la letra.
* En la página de ayuda aparece una breve explicación sobre el funcionamiento de la web.
* En la página de acceso al Admin Site aparece un formulario para iniciar sesión, introducimos el nombre de usuario y la contraseña y nos redirige a la página de administración de Django.

## Lista partes opcionales

* Inclusión de un favicon del sitio. Es el icono de la página web.
* Terminar sesión. En todas las páginas aparece la opción de terminar sesión que nos redirige a la página principal. El sitio web se comporta como si fuera la primera vez que se accede a él.
