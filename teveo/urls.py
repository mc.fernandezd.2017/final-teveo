from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('config/', views.configuracion, name='configuracion'),
    path('autorizar/', views.autorizar_navegador, name='autorizar_navegador'),
    path('ayuda/', views.ayuda, name='ayuda'),
    path('camaras/', views.camaras, name='camaras'),
    path('camara/<str:id_camara>/', views.camara, name='camara'),
    path('camara/<str:id_camara>/comentario/', views.comentario, name='comentario'),
    path('camara-dyn/<str:id_camara>/', views.camara_dinamica, name='camara_dinamica'),
    path('camara-dyn/<str:id_camara>/image', views.descargar_y_devolver_imagen, name='descargar_y_devolver_imagen'),
    path('camara-json/<str:id_camara>/', views.camara_json, name='camara_json'),
    path('terminar_sesion/', views.logout_view, name='logout_view'),
]
