from django.db import models
from django.conf import settings


# Create your models here.
class Configuracion(models.Model):
    nombre_comentador = models.CharField(max_length=100, default="Anónimo")
    tamanyo_letra = models.IntegerField()
    tipo_letra = models.CharField(max_length=20)


class Camera_listado1(models.Model):
    identifier = models.IntegerField(unique=True)
    image_url = models.URLField()
    name = models.CharField(max_length=100)
    coordinates = models.CharField(max_length=100)


class Camera_listado2(models.Model):
    identifier = models.CharField(max_length=100, unique=True)
    image_url = models.URLField()
    name = models.CharField(max_length=100)
    coordinates = models.CharField(max_length=100)


class Comentario_listado1(models.Model):
    name = models.CharField(max_length=100)
    camara1 = models.ForeignKey(Camera_listado1, on_delete=models.CASCADE)
    texto = models.CharField(max_length=300, blank=False)
    fecha = models.DateTimeField(auto_now_add=True)
    imagen = models.FilePathField(path=f'{settings.BASE_DIR}/static/imagen/', blank=False)

    def __str__(self):
        return f"Comentario en {self.camara1.name} - {self.fecha} - {self.texto}"


class Comentario_listado2(models.Model):
    name = models.CharField(max_length=100)  # nombre autor
    camara2 = models.ForeignKey(Camera_listado2, on_delete=models.CASCADE)
    texto = models.CharField(max_length=300, blank=False)
    fecha = models.DateTimeField(auto_now_add=True)
    imagen = models.FilePathField(path=f'{settings.BASE_DIR}/static/imagen/', blank=False)

    def __str__(self):
        return f"Comentario en {self.camara2.name} - {self.fecha} - {self.texto}"
