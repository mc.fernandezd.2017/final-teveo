from django.contrib import admin
from .models import Camera_listado1, Camera_listado2, Comentario_listado1, Comentario_listado2, Configuracion

# Register your models here.

admin.site.register(Camera_listado1)
admin.site.register(Camera_listado2)
admin.site.register(Comentario_listado1)
admin.site.register(Comentario_listado2)
admin.site.register(Configuracion)
