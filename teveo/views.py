import os.path
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Configuracion, Camera_listado1, Camera_listado2, Comentario_listado1, Comentario_listado2
import random, requests, base64
import xml.etree.ElementTree as ET
import urllib.request
from django.utils import timezone
from django.contrib.auth import logout
from django.conf import settings


# Create your views here.
@csrf_exempt
def index(request):
    # Recupera la configuración del sitio
    configuracion_actual = Configuracion.objects.first()
    nombre_comentador = "Anónimo"
    tamanyo_letra = None
    tipo_letra = None
    # Pasa el nombre del comentador al contexto de la plantilla
    if configuracion_actual:
        nombre_comentador = configuracion_actual.nombre_comentador
        tamanyo_letra = configuracion_actual.tamanyo_letra
        tipo_letra = configuracion_actual.tipo_letra
        if nombre_comentador == "":
            nombre_comentador = "Anónimo"

    comentarios_listado1 = Comentario_listado1.objects.all()
    comentarios_listado2 = Comentario_listado2.objects.all()
    # Combina los comentarios en una lista
    comentarios = list(comentarios_listado1) + list(comentarios_listado2)
    # Ordena los comentarios por fecha de publicación
    comentarios.sort(key=lambda comentario: comentario.fecha, reverse=True)

    numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
    numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

    contexto = {'nombre_comentador': nombre_comentador,
                'tamanyo_letra': tamanyo_letra,
                'tipo_letra': tipo_letra,
                'comentarios': comentarios,
                'numero_camaras': numero_camaras,
                'numero_comentarios': numero_comentarios}
    return render(request, 'index.html', contexto)


def download_data(file_name):
    # Descargar la fuente de datos y almacenar la información correspondiente en la base de datos
    if file_name == 'listado1.xml':
        url = 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml'
    elif file_name == 'listado2.xml':
        url = 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml'

    # Descarga el archivo XML
    response = urllib.request.urlopen(url)
    data = response.read().decode('utf-8')
    # Procesa el XML y almacena la información en la base de datos
    root = ET.fromstring(data)

    for camara in root.findall('camara'):
        if file_name == 'listado1.xml':
            id = camara.find('id').text
            src = camara.find('src').text
            lugar = camara.find('lugar').text
            coordenadas = camara.find('coordenadas').text

            # Verifica si la cámara ya existe en la base de datos antes de guardarla
            if not Camera_listado1.objects.filter(identifier=id).exists():
                Camera_listado1.objects.create(identifier=id,
                                               image_url=src,
                                               name=lugar,
                                               coordinates=coordenadas)

    for cam in root.findall('cam'):
        if file_name == 'listado2.xml':
            id = cam.get('id')
            url = cam.find('url').text
            info = cam.find('info').text
            latitude = cam.find('place/latitude').text
            longitude = cam.find('place/longitude').text

            if not Camera_listado2.objects.filter(identifier=id).exists():
                Camera_listado2.objects.create(identifier=id,
                                               image_url=url,
                                               name=info,
                                               coordinates=latitude + ',' + longitude)


def camaras(request):
    configuracion_actual = Configuracion.objects.first()
    nombre_comentador = "Anónimo"
    tamanyo_letra = None
    tipo_letra = None
    sources = {
        'listado1.xml': 'Listado 1',
        'listado2.xml': 'Listado 2'
    }

    if configuracion_actual:
        nombre_comentador = configuracion_actual.nombre_comentador
        tamanyo_letra = configuracion_actual.tamanyo_letra
        tipo_letra = configuracion_actual.tipo_letra
        if nombre_comentador == "":
            nombre_comentador = "Anónimo"

    if request.method == 'POST':
        file_name = request.POST.get('file_name')
        download_data(file_name)

    cameras = Camera_listado1.objects.all()
    cameras2 = Camera_listado2.objects.all()

    if cameras:
        camara_aleatoria1 = random.choice(cameras)
    else:
        camara_aleatoria1 = None

    if cameras2:
        camara_aleatoria2 = random.choice(cameras2)
    else:
        camara_aleatoria2 = None

    # Luego, selecciona una cámara aleatoria de las dos listas si alguna de ellas tiene elementos
    if camara_aleatoria1 and camara_aleatoria2:
        camara_aleatoria = random.choice([camara_aleatoria1, camara_aleatoria2])
    elif camara_aleatoria1:
        camara_aleatoria = camara_aleatoria1
    elif camara_aleatoria2:
        camara_aleatoria = camara_aleatoria2
    else:
        camara_aleatoria = None

    numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
    numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

    numero_comentarios_cameras = []
    for camera in cameras:
        numero_comentarios_cameras.append((camera.identifier,
                                           Comentario_listado1.objects.filter(camara1=camera).count()))
    for camera in cameras2:
        numero_comentarios_cameras.append((camera.identifier,
                                           Comentario_listado2.objects.filter(camara2=camera).count()))

    # Ordena la lista de tuplas por el número de comentarios
    numero_comentarios_cameras.sort(key=lambda x: x[1], reverse=True)

    contexto = {'nombre_comentador': nombre_comentador,
                'tamanyo_letra': tamanyo_letra,
                'tipo_letra': tipo_letra,
                'sources': sources,
                'cameras': cameras,
                'cameras2': cameras2,
                'camara_aleatoria': camara_aleatoria,
                'numero_camaras': numero_camaras,
                'numero_comentarios': numero_comentarios,
                'numero_comentarios_cameras': numero_comentarios_cameras}
    return render(request, 'camaras.html', contexto)


@csrf_exempt
def camara(request, id_camara):
    configuracion_actual = Configuracion.objects.first()
    nombre_comentador = "Anónimo"
    tamanyo_letra = None
    tipo_letra = None
    if configuracion_actual:
        nombre_comentador = configuracion_actual.nombre_comentador
        tamanyo_letra = configuracion_actual.tamanyo_letra
        tipo_letra = configuracion_actual.tipo_letra
        if nombre_comentador == "":
            nombre_comentador = "Anónimo"

    if id_camara.isdigit():
        camera = Camera_listado1.objects.get(identifier=id_camara)
        if id_camara == 1 or id_camara == 2 or id_camara == 3 or id_camara == 4:
            id = camera.find('id').text
            src = camera.find('src').text
            lugar = camera.find('lugar').text
            coordenadas = camera.find('coordenadas').text
            Camera_listado1.objects.create(identifier=id,
                                           image_url=src,
                                           name=lugar,
                                           coordinates=coordenadas)

        comentarios = Comentario_listado1.objects.filter(camara1=id_camara).order_by('-fecha')
        numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
        numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

        contexto = {'nombre_comentador': nombre_comentador,
                    'tamanyo_letra': tamanyo_letra,
                    'tipo_letra': tipo_letra,
                    'camera': camera,
                    'comentarios': comentarios,
                    'numero_camaras': numero_camaras,
                    'numero_comentarios': numero_comentarios}
        return render(request, 'camara.html', contexto)
    else:
        camera = Camera_listado2.objects.get(identifier=id_camara)
        if id_camara == 'A' or id_camara == 'B' or id_camara == 'C':
            id = camera.identifier
            url = camera.image_url
            info = camera.name
            latitude = camera.coordinates.split(',')[0]
            longitude = camera.coordinates.split(',')[1]

        comentarios = Comentario_listado2.objects.filter(camara2__identifier=id_camara).order_by('-fecha')
        numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
        numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

        contexto = {'nombre_comentador': nombre_comentador,
                    'tamanyo_letra': tamanyo_letra,
                    'tipo_letra': tipo_letra,
                    'camera': camera,
                    'comentarios': comentarios,
                    'numero_camaras': numero_camaras,
                    'numero_comentarios': numero_comentarios}
        return render(request, 'camara.html', contexto)


def download_image(url, file_name):
    file_path = os.path.join(settings.BASE_DIR, 'teveo/static/imagen', file_name)
    urllib.request.urlretrieve(url, file_path)


def comentario(request, id_camara):
    configuracion_actual = Configuracion.objects.first()
    nombre_comentador = "Anónimo"
    tamanyo_letra = None
    tipo_letra = None
    if configuracion_actual:
        nombre_comentador = configuracion_actual.nombre_comentador
        tamanyo_letra = configuracion_actual.tamanyo_letra
        tipo_letra = configuracion_actual.tipo_letra
        if nombre_comentador == "":
            nombre_comentador = "Anónimo"

    if id_camara.isdigit():
        camera = Camera_listado1.objects.get(identifier=id_camara)
        i = 0
        img = f'imagen{camera.identifier}.{i}.jpg'
        comment = Comentario_listado1.objects.filter(imagen=img)
        while comment.exists():
            i += 1
            img = f'imagen{camera.identifier}.{i}.jpg'
            comment = Comentario_listado1.objects.filter(imagen=img)
        download_image(camera.image_url, img)
        if request.method == 'POST':
            texto_comentario = request.POST.get('texto')
            fecha = timezone.now()
            comentario = Comentario_listado1(name=nombre_comentador,
                                             camara1=camera,
                                             texto=texto_comentario,
                                             fecha=fecha,
                                             imagen=img)
            comentario.save()
            return redirect('/camara/' + id_camara + '/')

        numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
        numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

        contexto = {'nombre_comentador': nombre_comentador,
                    'tamanyo_letra': tamanyo_letra,
                    'tipo_letra': tipo_letra,
                    'camera': camera,
                    'now': timezone.now(),
                    'numero_camaras': numero_camaras,
                    'numero_comentarios': numero_comentarios}
        return render(request, 'comentario.html', contexto)
    else:
        camera = Camera_listado2.objects.get(identifier=id_camara)
        i = 0
        img = f'imagen{camera.identifier}.{i}.jpg'
        comment = Comentario_listado2.objects.filter(imagen=img)
        while comment.exists():
            i += 1
            img = f'imagen{camera.identifier}.{i}.jpg'
            comment = Comentario_listado2.objects.filter(imagen=img)
        download_image(camera.image_url, img)
        if request.method == 'POST':
            texto_comentario = request.POST.get('texto')
            fecha = timezone.now()
            comentario = Comentario_listado2(name=nombre_comentador,
                                             camara2=camera,
                                             texto=texto_comentario,
                                             fecha=fecha,
                                             imagen=img)
            comentario.save()
            return redirect('/camara/' + id_camara + '/')

        numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
        numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

        contexto = {'nombre_comentador': nombre_comentador,
                    'tamanyo_letra': tamanyo_letra,
                    'tipo_letra': tipo_letra,
                    'camera': camera,
                    'now': timezone.now(),
                    'numero_camaras': numero_camaras,
                    'numero_comentarios': numero_comentarios}
        return render(request, 'comentario.html', contexto)


def descargar_y_devolver_imagen(request, id_camara):
    try:
        if id_camara.isdigit():
            camera1 = Camera_listado1.objects.all()
            camera = camera1.get(identifier=id_camara)
            # URL de la imagen a descargar
            url_imagen = camera.image_url

            # Realizar la solicitud HTTP para obtener la imagen
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
            }
            # Realizar la solicitud HTTP para obtener la imagen
            response = requests.get(url_imagen, headers=headers)
            response.raise_for_status()  # Lanza una excepción si la solicitud no fue exitosa

            # Extraer el contenido de la respuesta
            imagen_bytes = response.content
            # Convertir la imagen a formato Base64
            imagen_base64 = base64.b64encode(response.content).decode('utf-8')

            devuelvo = '<img src="data:image/jpeg;base64,##content##">'
            devuelvo = devuelvo.replace('##content##', imagen_base64)
            # Devolver la imagen como una HttpResponse con el tipo de contenido adecuado
            return HttpResponse(devuelvo, content_type="image/jpeg")
        else:
            camera2 = Camera_listado2.objects.all()
            camera = camera2.get(identifier=id_camara)
            # URL de la imagen a descargar
            url_imagen = camera.image_url

            # Realizar la solicitud HTTP para obtener la imagen
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
            }
            # Realizar la solicitud HTTP para obtener la imagen
            response = requests.get(url_imagen, headers=headers)
            response.raise_for_status()  # Lanza una excepción si la solicitud no fue exitosa

            # Extraer el contenido de la respuesta
            imagen_bytes = response.content
            # Convertir la imagen a formato Base64
            imagen_base64 = base64.b64encode(response.content).decode('utf-8')

            devuelvo = '<img src="data:image/jpeg;base64,##content##">'
            devuelvo = devuelvo.replace('##content##', imagen_base64)
            # Devolver la imagen como una HttpResponse con el tipo de contenido adecuado
            return HttpResponse(devuelvo, content_type="image/jpeg")
    except Exception as e:
        return HttpResponse(str(e), status=500)


def camara_dinamica(request, id_camara):
    configuracion_actual = Configuracion.objects.first()
    nombre_comentador = "Anónimo"
    tamanyo_letra = None
    tipo_letra = None
    if configuracion_actual:
        nombre_comentador = configuracion_actual.nombre_comentador
        tamanyo_letra = configuracion_actual.tamanyo_letra
        tipo_letra = configuracion_actual.tipo_letra
        if nombre_comentador == "":
            nombre_comentador = "Anónimo"

    if request.method == 'GET':
        if id_camara.isdigit():
            camera = Camera_listado1.objects.get(identifier=id_camara)
            comentarios = Comentario_listado1.objects.filter(camara1=id_camara).order_by('-fecha')
            numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
            numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

            contexto = {'nombre_comentador': nombre_comentador,
                        'tamanyo_letra': tamanyo_letra,
                        'tipo_letra': tipo_letra,
                        'camera': camera,
                        'comentarios': comentarios,
                        'numero_camaras': numero_camaras,
                        'numero_comentarios': numero_comentarios}

            return render(request, 'camara_dinamica.html', contexto)
        else:
            camera = Camera_listado2.objects.get(identifier=id_camara)
            comentarios = Comentario_listado2.objects.filter(camara2__identifier=id_camara).order_by('-fecha')
            numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
            numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

            contexto = {'nombre_comentador': nombre_comentador,
                        'tamanyo_letra': tamanyo_letra,
                        'tipo_letra': tipo_letra,
                        'camera': camera,
                        'comentarios': comentarios,
                        'numero_camaras': numero_camaras,
                        'numero_comentarios': numero_comentarios}

            return render(request, 'camara_dinamica.html', contexto)


def camara_json(request, id_camara):
    if id_camara.isdigit():
        cameras_data = []
        cameras_listado1 = Camera_listado1.objects.filter(identifier=id_camara)
        numero_comentarios_cameras = Comentario_listado1.objects.filter(camara1=id_camara).count()
        for camera in cameras_listado1:
            cameras_listado1_dict = {'id': camera.identifier,
                                     'src': camera.image_url,
                                     'lugar': camera.name,
                                     'coordenadas': camera.coordinates,
                                     'numero_comentarios': numero_comentarios_cameras}
            cameras_data.append(cameras_listado1_dict)
            return JsonResponse(cameras_data, safe=False)
    else:
        cameras_data = []
        cameras_listado2 = Camera_listado2.objects.filter(identifier=id_camara)
        numero_comentarios_cameras = Comentario_listado2.objects.filter(camara2__identifier=id_camara).count()
        for camera in cameras_listado2:
            cameras_listado2_dict = {'id': camera.identifier,
                                     'url': camera.image_url,
                                     'info': camera.name,
                                     'place': camera.coordinates,
                                     'numero_comentarios': numero_comentarios_cameras}
            cameras_data.append(cameras_listado2_dict)
            return JsonResponse(cameras_data, safe=False)


@csrf_exempt
def configuracion(request):
    session_key = request.session.session_key
    nombre_comentador = "Anónimo"
    configuracion_actual = Configuracion.objects.first()

    if request.method == 'POST':
        nombre_comentador = request.POST.get('nombre_comentador')
        tamanyo_letra = request.POST.get('tamanyo_letra')
        tipo_letra = request.POST.get('tipo_letra')

        if tamanyo_letra == "":
            tamanyo_letra = 20

        if configuracion_actual:
            configuracion_actual.nombre_comentador = nombre_comentador
            configuracion_actual.tamanyo_letra = tamanyo_letra
            configuracion_actual.tipo_letra = tipo_letra
            if nombre_comentador == "":
                configuracion_actual.nombre_comentador = "Anónimo"
            if tamanyo_letra == "":
                configuracion_actual.tamanyo_letra = 20
            configuracion_actual.save()
        else:
            Configuracion.objects.create(nombre_comentador=nombre_comentador,
                                         tamanyo_letra=tamanyo_letra,
                                         tipo_letra=tipo_letra)

        return redirect('/')

    numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
    numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

    contexto = {'configuracion_actual': configuracion_actual,
                'session_key': session_key,
                'numero_camaras': numero_camaras,
                'numero_comentarios': numero_comentarios}
    return render(request, 'configuracion.html', contexto)


def autorizar_navegador(request):
    if 'session_key' in request.GET:
        session_key = request.GET['session_key']
        request.session['authorized_session_key'] = session_key

        return HttpResponse("Se ha autorizado al navegador correctamente.")
    return HttpResponse("Error: No se proporcionó el identificador de sesión.")


def ayuda(request):
    configuracion_actual = Configuracion.objects.first()
    nombre_comentador = "Anónimo"
    tamanyo_letra = None
    tipo_letra = None
    if configuracion_actual:
        nombre_comentador = configuracion_actual.nombre_comentador
        tamanyo_letra = configuracion_actual.tamanyo_letra
        tipo_letra = configuracion_actual.tipo_letra
        if nombre_comentador == "":
            nombre_comentador = "Anónimo"

    numero_camaras = Camera_listado1.objects.count() + Camera_listado2.objects.count()
    numero_comentarios = Comentario_listado1.objects.count() + Comentario_listado2.objects.count()

    contexto = {'nombre_comentador': nombre_comentador,
                'tamanyo_letra': tamanyo_letra,
                'tipo_letra': tipo_letra,
                'numero_camaras': numero_camaras,
                'numero_comentarios': numero_comentarios}
    return render(request, 'ayuda.html', contexto)


def logout_view(request):
    Configuracion.objects.all().delete()
    Comentario_listado1.objects.all().delete()
    Comentario_listado2.objects.all().delete()
    logout(request)
    request.session.flush()
    return redirect('/')
