from django.test import TestCase
from .models import Camera_listado1, Camera_listado2


class Tests(TestCase):
    def test_index(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_ayuda(self):
        response = self.client.get('/ayuda/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'ayuda.html')

    def test_configuracion(self):
        response = self.client.get('/config/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'configuracion.html')
        response = self.client.post('', {'nombre_comentador': 'pepe',
                                         'tamanyo_letra': 40,
                                         'tipo_letra': 'Times New Roman'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_camaras(self):
        response = self.client.get('/camaras/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'camaras.html')

    def test_camara(self):
        for camera in Camera_listado1.objects.all():
            response = self.client.get('/camara/' + camera.identifier + '/')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'camara.html')
            self.assertTemplateNotUsed(response, 'camara_dinamica.html')

        for camera in Camera_listado2.objects.all():
            response = self.client.get('/camara/' + camera.identifier + '/')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'camara.html')
            self.assertTemplateNotUsed(response, 'camara_dinamica.html')

    def test_camara_dinamica(self):
        for camera in Camera_listado1.objects.all():
            response = self.client.get('/camara-dyn/' + camera.identifier + '/')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'camara_dinamica.html')
            self.assertTemplateNotUsed(response, 'camara.html')

        for camera in Camera_listado2.objects.all():
            response = self.client.get('/camara-dyn/' + camera.identifier + '/')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'camara_dinamica.html')
            self.assertTemplateNotUsed(response, 'camara.html')

    def test_camara_dinamica_imagen(self):
        for camera in Camera_listado1.objects.all():
            response = self.client.get('/camara-dyn/' + camera.identifier + '/image')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response['Content-Type'], 'image/jpeg')

        for camera in Camera_listado2.objects.all():
            response = self.client.get('/camara-dyn/' + camera.identifier + '/image')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response['Content-Type'], 'image/jpeg')

    def test_camara_json(self):
        for camera in Camera_listado1.objects.all():
            response = self.client.get('/camara-json/' + camera.identifier + '/')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response['Content-Type'], 'application/json')

        for camera in Camera_listado2.objects.all():
            response = self.client.get('/camara-dyn/' + camera.identifier + '/')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response['Content-Type'], 'application/json')

    def test_comentario(self):
        for camera in Camera_listado1.objects.all():
            response = self.client.get('/camara/' + camera.identifier + '/comentario/')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'comentario.html')
            response = self.client.post('/camara/' + camera.identifier + '/', {'texto': 'Prueba'})
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'camara.html')

        for camera in Camera_listado2.objects.all():
            response = self.client.get('/camara/' + camera.identifier + '/comentario')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'comentario.html')
            response = self.client.post('/camara/' + camera.identifier + '/', {'texto': 'Prueba 2'})
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'camara.html')
